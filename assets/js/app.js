$( document ).ready(function () {
  var conn;

  var options = {
    zone: document.getElementById('stick'),
    color: 'blue'
  };
  var manager = nipplejs.create(options);
  const ON = "ON"
  const OFF = "OFF"
  const UP = "UP"
  const DOWN = "DOWN"
  const FIRE = "FIRE"
  var shooter = OFF


  connectRobot()

  var stickTimer;

  manager.on("start", function(evt, nipple){
    console.log('down')
    stickTimer = setInterval(function(){
      x = nipple.frontPosition.x * 2
      y = nipple.frontPosition.y * 2

      console.log(nipple.frontPosition.x + " " + nipple.frontPosition.y)
  
      t = calculateTank(x, y);
      sendCommand({motor: t})
    }, 1/30 * 1000);
  })

  manager.on("end", function(){
    console.log('up')
    clearInterval(stickTimer)
    stopRobot()
  })

  $( "#shooter")
  .click(function() {
    console.log("Shooter Change")
    if(shooter == ON) {
      setShooter(OFF)
      shooter = OFF
    } else if (shooter == OFF) {
      setShooter(ON)
      shooter = ON
    }
  })

  function connectRobot(){
    console.log("Connect websocket")
    if (window["WebSocket"]) {
      if (conn == undefined || conn.readyState == conn.CLOSE) {
        conn = new WebSocket("ws://" + window.location.host + "/ws");
        conn.onopen = function(event) {
          setShooter(OFF)
          $("#discon").hide()
        }
        conn.onclose = function (evt) {
            console.log("Connection closed.");
            $("#discon").show()
            connectRobot()
        };
      }
    } else {
      alert("Sorry, your browser doesn't support Device Orientation");
    }
  }

  function stopRobot(){
    sendCommand({motor: {left: 0, right: 0}})
  }

  function setShooter(state){
    sendCommand({shooter: state})
    console.log("Shooter state: " + state)
    if(state == "ON"){
      $("#shooter").removeClass("btn-primary")
      $("#shooter").addClass("btn-danger")
      $("#shooter-fire").attr("disabled", false);
    } else {
      $("#shooter").removeClass("btn-danger")
      $("#shooter").addClass("btn-primary")
      $("#shooter-fire").attr("disabled", true);
    }
  }

  $("#shooter-up").click(function(){sendCommand({shooterCommand: UP})})
  $("#shooter-down").click(function(){sendCommand({shooterCommand: DOWN})})
  $("#shooter-fire").click(function(){sendCommand({shooterCommand: FIRE})})

  function sendCommand(command) {
    var message = JSON.stringify(command);
    if (!conn || conn.readyState != conn.OPEN) {
      connectRobot()
    }
    console.log("Send: " + message)
    conn.send(message);
  }

  function calculateTank(x, y){
    // First hypotenuse
    var z = Math.sqrt(x*x + y*y);
    // angle in radians
    rad = Math.acos(Math.abs(x)/z);
    // and in degrees
    angle = rad*180/Math.PI; // TODO: Add Degrees from joystick
    // Now angle indicates the measure of turn
    // Along a straight line, with an angle o, the turn co-efficient is same
    // this applies for angles between 0-90, with angle 0 the co-eff is -1
    // with angle 45, the co-efficient is 0 and with angle 90, it is 1
    var tcoeff = -1 + (angle/90)*2;
    var turn = tcoeff * Math.abs(Math.abs(y) - Math.abs(x));
    turn = Math.round(turn*100)/100;
    // And max of y or x is the movement
    var move = Math.max(Math.abs(y),Math.abs(x));

    // First and third quadrant
    if( (x >= 0 && y >= 0) || (x < 0 &&  y < 0) ) {
        left = move;
        right = turn;
    } else {
        right = move;
        left = turn;
    }

    // Reverse polarity
    if(y > 0) {
        left = 0 - left;
        right = 0 - right;
    }

    return { left: left / 100, right: right / 100}
  }
});
