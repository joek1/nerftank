package nerftank_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestNerfTank(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "NerfTank Suite")
}
