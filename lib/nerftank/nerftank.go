package nerftank

import (
	"log"
	"sync"
	"time"

	"gobot.io/x/gobot/drivers/gpio"

	"github.com/joek/picoborgrev"
	"gobot.io/x/gobot/drivers/i2c"
)

// NerfTank driver interace
type NerfTank interface {
	Name() string
	Start() []error
	Halt() []error
	SetMotorLeft(float32) error
	SetMotorRight(float32) error
	CanonUp(uint8) error
	CanonDown(uint8) error
	SetMaxCanon(uint8)
	SetMinCanon(uint8)
	Shoot()
	SetShooterMax(uint8)
	SetShooterMin(uint8)
	StartShooter()
	StopShooter()
	SetShooterSpeed(uint8)
}

// Shooter interface to controll the shooter speed
type Shooter interface {
	SetSpeed(uint8) error
}

// ShooterDriver is the connector to controll the shooter speed
type ShooterDriver struct {
	ServoBoard *i2c.PCA9685Driver
	Pin        string
}

// SetSpeed is setting the rotating speed of shooter
func (s *ShooterDriver) SetSpeed(i uint8) (err error) {
	return s.ServoBoard.PwmWrite(s.Pin, i)
}

// Driver struct
type Driver struct {
	name         string
	motor        picoborgrev.RevDriver
	nick         *gpio.ServoDriver
	shoot        *gpio.ServoDriver
	shooter      Shooter
	canon        uint8
	maxCanon     uint8
	minCanon     uint8
	maxShooter   uint8
	minShooter   uint8
	shooterSpeed uint8
	i2c.Config
	lock sync.Mutex
}

// NewDriver creates a new beerbot driver with specified name and i2c interface and MotorController adresses
func NewDriver(
	name string,
	motor picoborgrev.RevDriver,
	nick *gpio.ServoDriver,
	shoot *gpio.ServoDriver,
	shooter Shooter,
	options ...func(i2c.Config)) *Driver {
	return &Driver{
		name:         name,
		Config:       i2c.NewConfig(),
		motor:        motor,
		nick:         nick,
		shoot:        shoot,
		shooter:      shooter,
		canon:        58,
		maxCanon:     81,
		minCanon:     30,
		maxShooter:   180,
		minShooter:   0,
		shooterSpeed: 254,
		lock:         sync.Mutex{},
	}
}

// Name is giving the robot name
func (d *Driver) Name() string {
	return d.name
}

// SetName is setting the robot name
func (d *Driver) SetName(n string) {
	d.name = n
}

// Start is starting the robot
func (d *Driver) Start() (err error) {
	d.lock.Lock()
	defer d.lock.Unlock()

	d.nick.Move(d.canon)
	d.shoot.Move(d.minShooter)
	d.shooter.SetSpeed(0)

	err = d.motor.Start()
	if err != nil {
		return
	}

	err = d.motor.ResetEPO()
	if err != nil {
		return
	}

	return
}

// Halt is stopping the robot
func (d *Driver) Halt() (err error) {
	log.Println("Stop Motors")
	d.lock.Lock()
	defer d.lock.Unlock()

	err = d.motor.Halt()
	if err != nil {
		return
	}
	log.Println("Stop Motors")

	d.shooter.SetSpeed(0)

	log.Println("Stop Shooter")

	return
}

// SetMotorLeft is setting motor speed of left motor
func (d *Driver) SetMotorLeft(p float32) error {
	d.lock.Lock()
	defer d.lock.Unlock()

	err := d.motor.SetMotorA(p)
	if err != nil {
		return err
	}

	return nil
}

// SetMotorRight is setting motor speed of right motor
func (d *Driver) SetMotorRight(p float32) error {
	d.lock.Lock()
	defer d.lock.Unlock()

	p = p * (-1)

	err := d.motor.SetMotorB(p)
	if err != nil {
		return err
	}

	return nil
}

// CanonUp is moving the canon up by `angel`
func (d *Driver) CanonUp(angel uint8) (err error) {
	d.canon = d.canon + angel
	if d.canon <= d.maxCanon {
		err = d.nick.Move(d.canon)
	}
	return
}

// CanonDown is moving the canon down by `angel`
func (d *Driver) CanonDown(angel uint8) (err error) {
	d.canon = d.canon - angel
	if d.canon >= d.minCanon {
		err = d.nick.Move(d.canon)
	}
	return
}

// SetMaxCanon is setting the maximum hight the canon can be pushed to.
func (d *Driver) SetMaxCanon(angel uint8) {
	d.maxCanon = angel
}

// SetMinCanon is setting the minimum hight the canon can be pushed to.
func (d *Driver) SetMinCanon(angel uint8) {
	d.minCanon = angel
}

// Shoot is firing the canon
func (d *Driver) Shoot() (err error) {
	go func() {
		d.shoot.Move(d.maxShooter)
		time.Sleep(1 * time.Second)
		d.shoot.Move(d.minShooter)
	}()
	return
}

// SetShooterMax is setting the max boundary for the shooter
func (d *Driver) SetShooterMax(m uint8) {
	d.maxShooter = m
}

// SetShooterMin is setting the min boundary for the shooter
func (d *Driver) SetShooterMin(m uint8) {
	d.minShooter = m
}

// StartShooter is spinning the shooter
func (d *Driver) StartShooter() (err error) {
	err = d.shooter.SetSpeed(d.shooterSpeed)
	return err
}

// StopShooter is stop spinning the shooter
func (d *Driver) StopShooter() (err error) {
	err = d.shooter.SetSpeed(0)
	return err
}

// SetShooterSpeed is setting the spinning speed of the shooter
func (d *Driver) SetShooterSpeed(s uint8) {
	d.shooterSpeed = s
}
