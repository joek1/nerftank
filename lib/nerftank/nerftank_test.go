package nerftank_test

import (
	"time"

	"github.com/joek/picoborgrev/revtesthelpers"
	. "gitlab.com/joek1/nerftank/lib/nerftank"
	"gobot.io/x/gobot/drivers/gpio"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("NerfTank", func() {
	var adaptor *gpioTestAdaptor
	var motor *revtesthelpers.FakeRevDriver
	var nick *gpio.ServoDriver
	var shoot *gpio.ServoDriver
	var shooter *shooterTestDriver

	BeforeEach(func() {
		adaptor = newGpioTestAdaptor()
		motor = revtesthelpers.NewFakeRevDriver()
		nick = gpio.NewServoDriver(adaptor, "26")
		shoot = gpio.NewServoDriver(adaptor, "27")
		shooter = newShooterTestDriver()
	})

	It("Creates a new Driver instance", func() {
		var d *Driver
		d = NewDriver("Test", motor, nick, shoot, shooter)
		Ω(d).Should(BeAssignableToTypeOf(&Driver{}))
	})

	It("Is starting the robot", func() {
		m1 := false
		epo1 := false
		motor.StartImpl = func() error {
			m1 = true
			return nil
		}

		motor.ResetEPOImpl = func() error {
			epo1 = true
			return nil
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.Start()

		Ω(m1).Should(BeTrue())
		Ω(epo1).Should(BeTrue())

	})

	It("Is stopping the robot", func() {
		stop1 := false
		motor.HaltImpl = func() error {
			stop1 = true
			return nil
		}

		s := false
		shooter.testSetSpeed = func(i uint8) error {
			s = true
			Ω(i).Should(Equal(uint8(0)))
			return nil
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.Halt()

		Ω(stop1).Should(BeTrue())
		Ω(s).Should(BeTrue())
	})

	It("Is returning name", func() {

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.Halt()

		Ω(d.Name()).Should(Equal("Test"))
	})

	It("Is setting left Motors", func() {
		var m1 float32
		motor.SetMotorAImpl = func(p float32) error {
			m1 = p
			return nil
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.SetMotorLeft(0.32)

		Ω(m1).Should(Equal(float32(0.32)))
	})

	It("Is setting right Motors", func() {
		var m1 float32
		motor.SetMotorBImpl = func(p float32) error {
			m1 = p
			return nil
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.SetMotorRight(0.32)

		Ω(m1).Should(Equal(float32(-0.32)))
	})

	It("Is balancing the canon at startup", func() {
		s := 0
		adaptor.testAdaptorServoWrite = func(p string, b byte) (err error) {
			s++
			switch s {
			case 1:
				Ω(p).Should(Equal("26"))
				Ω(b).Should(Equal(uint8(58)))
				break
			default:
				break
			}

			return
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		err := d.Start()

		Ω(err).ShouldNot(HaveOccurred())

		Ω(s).Should(Equal(2))
	})

	It("Is raising the canon", func() {
		s := 0
		adaptor.testAdaptorServoWrite = func(p string, b byte) (err error) {
			s++

			switch s {
			case 1:
				Ω(p).Should(Equal("26"))
				Ω(b).Should(Equal(uint8(58)))
				break
			case 3:
				Ω(p).Should(Equal("26"))
				Ω(b).Should(Equal(uint8(63)))
				break
			}
			return
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.Start()
		err := d.CanonUp(5)

		Ω(err).ShouldNot(HaveOccurred())
		Ω(s).Should(Equal(3))
	})
	It("Is lowering the canon", func() {
		s := 0
		adaptor.testAdaptorServoWrite = func(p string, b byte) (err error) {
			s++

			switch s {
			case 1:
				Ω(p).Should(Equal("26"))
				Ω(b).Should(Equal(uint8(58)))
				break
			case 3:
				Ω(p).Should(Equal("26"))
				Ω(b).Should(Equal(uint8(53)))
				break
			}
			return
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.Start()
		err := d.CanonDown(5)

		Ω(err).ShouldNot(HaveOccurred())
		Ω(s).Should(Equal(3))
	})

	It("Is stopping the canon at max", func() {
		s := 0
		adaptor.testAdaptorServoWrite = func(p string, b byte) (err error) {
			s++
			return
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.Start()

		d.SetMaxCanon(100)
		err := d.CanonUp(80)

		Ω(err).ShouldNot(HaveOccurred())
		Ω(s).Should(Equal(2))
	})
	It("Is stopping the canon at min", func() {
		s := 0
		adaptor.testAdaptorServoWrite = func(p string, b byte) (err error) {
			s++
			return
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.Start()
		d.SetMinCanon(50)
		err := d.CanonDown(50)

		Ω(err).ShouldNot(HaveOccurred())
		Ω(s).Should(Equal(2))
	})
	It("Is shooting", func() {
		s := 0
		adaptor.testAdaptorServoWrite = func(p string, b byte) (err error) {
			defer GinkgoRecover()
			s++
			Ω(p).Should(Equal("27"))
			switch s {
			case 1:
				Ω(b).Should(Equal(uint8(160)))
				break
			case 2:
				Ω(b).Should(Equal(uint8(5)))
				break
			}
			return
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)

		d.SetShooterMax(160)
		d.SetShooterMin(5)

		err := d.Shoot()
		Ω(err).ShouldNot(HaveOccurred())
		Eventually(func() int { return s }, 4*time.Second, 1*time.Second).Should(Equal(2))
	})

	It("Is starting shooter", func() {
		s := false
		shooter.testSetSpeed = func(i uint8) error {
			s = true
			Ω(i).Should(Equal(uint8(25)))
			return nil
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.SetShooterSpeed(25)
		d.StartShooter()

		Ω(s).Should(BeTrue())
	})

	It("Is stoping shooter", func() {
		s := false
		shooter.testSetSpeed = func(i uint8) error {
			s = true
			Ω(i).Should(Equal(uint8(0)))
			return nil
		}

		d := NewDriver("Test", motor, nick, shoot, shooter)
		d.StopShooter()

		Ω(s).Should(BeTrue())
	})

})
