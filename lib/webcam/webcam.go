package webcam

import (
	"fmt"
	"log"

	"github.com/hybridgroup/mjpeg"
	"gocv.io/x/gocv"
)

// init webcam (parameters for cmd)
// http handler

// Camera is a webcam streamer
type Camera struct {
	deviceID int
	webcam   *gocv.VideoCapture
	stream   *mjpeg.Stream
}

// NewCamera is creating a new camera instance using the webcam id.
func NewCamera(deviceID int) Camera {
	return Camera{
		deviceID: deviceID,
	}
}

// Stream is returning the webcam stream.
func (c *Camera) Stream() *mjpeg.Stream {
	return c.stream
}

// Start is start capturing images from the camera.
func (c *Camera) Start(flip bool) {
	// open webcam
	w, err := gocv.OpenVideoCapture(c.deviceID)
	if err != nil {
		fmt.Printf("Error opening capture device: %v\n", c.deviceID)
		return
	}

	c.webcam = w
	c.stream = mjpeg.NewStream()

	go c.mjpegCapture(flip)

	fmt.Println("Capturing.")
}

// Stop is stopping the camera
func (c *Camera) Stop() {
	log.Println("Stopping Webcam")
	c.webcam.Close()
}

func (c *Camera) mjpegCapture(flipImage bool) {
	img := gocv.NewMat()
	defer img.Close()
	destImg := gocv.NewMat()
	defer destImg.Close()

	for {
		if ok := c.webcam.Read(&img); !ok {
			fmt.Printf("Device closed: %v\n", c.deviceID)
			return
		}
		if img.Empty() {
			continue
		}
		if flipImage {
			gocv.Rotate(img, &destImg, gocv.Rotate180Clockwise)
		} else {
			destImg = img
		}

		buf, _ := gocv.IMEncode(".jpg", destImg)
		c.stream.UpdateJPEG(buf)
	}
}
