package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gobuffalo/packr"
	"github.com/joek/picoborgrev"
	"github.com/joek/robotwebhandlers/ws"
	"gitlab.com/joek1/nerftank/lib/nerftank"
	"gitlab.com/joek1/nerftank/lib/webcam"

	"gobot.io/x/gobot"
	"gobot.io/x/gobot/drivers/gpio"
	"gobot.io/x/gobot/drivers/i2c"
	"gobot.io/x/gobot/platforms/raspi"
)

const (
	up   = "UP"
	down = "DOWN"
	fire = "FIRE"
	on   = "ON"
	off  = "OFF"
)

func main() {
	var addr = flag.String("addr", ":8080", "http service address")
	var webcamID = flag.Int("webcamID", 0, "Webcam device ID.")
	var webcamEnabled = flag.Bool("webcam", true, "Enable Webcam.")

	flag.Parse()

	com := make(chan *ws.BotCommand)
	h := ws.NewHub(com)
	go h.Run()

	r := raspi.NewAdaptor()
	d := i2c.NewPCA9685Driver(r)
	motor := picoborgrev.NewDriver(r)
	nick := gpio.NewServoDriver(d, "0") // BCM 17
	shoot := gpio.NewServoDriver(d, "1")
	shooter := &nerftank.ShooterDriver{
		Pin:        "2",
		ServoBoard: d,
	}
	tank := nerftank.NewDriver("rev", motor, nick, shoot, shooter)

	var gracefulStop = make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)

	work := func() {
		d.SetPWMFreq(50)

		go func() {
			for {
				select {
				case c := <-com:
					if c.Motor != nil {
						tank.SetMotorLeft(c.Motor.Left)
						tank.SetMotorRight(c.Motor.Right)
					}

					if c.Shooter == on {
						log.Println("Shooter ON")
						tank.StartShooter()
					}

					if c.Shooter == off {
						log.Println("Shooter OFF")
						tank.StopShooter()
					}

					if c.ShooterCommand == up {
						tank.CanonUp(5)
					}

					if c.ShooterCommand == down {
						tank.CanonDown(5)
					}

					if c.ShooterCommand == fire {
						tank.Shoot()
					}

					if c.Event == "Disconnect" {
						tank.Halt()
					}
				case <-gracefulStop:
					log.Println("Stop Work")
					return
				}
			}
		}()
	}

	robot := gobot.NewRobot("nerftank",
		[]gobot.Connection{r},
		[]gobot.Device{motor, d, nick, shoot},
		work,
	)

	go robot.Start()
	defer robot.Stop()

	log.Println("Robot started")

	mux := http.NewServeMux()
	srv := &http.Server{
		Addr:    *addr,
		Handler: mux,
	}

	if *webcamEnabled {
		w := webcam.NewCamera(*webcamID)
		w.Start(true)
		defer w.Stop()
		mux.Handle("/webcam", w.Stream())
	}
	mux.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) { h.ServeWs(w, r) })
	assets := packr.NewBox("./assets")
	mux.Handle("/", http.FileServer(assets))

	log.Println("Start webserver")

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			log.Fatal("ListenAndServe: ", err)
		}
	}()

	<-gracefulStop

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*1)
	defer cancel()

	err := srv.Shutdown(ctx)
	if err != nil {
		log.Println(err)
	}

	// - Sensor output (broadcast)
}

// TODO: Follow me action
// TODO: Beleuchtung / Nightrider
