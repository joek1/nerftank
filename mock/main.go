package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gobuffalo/packr"

	"github.com/joek/robotwebhandlers/ws"
	"gitlab.com/joek1/nerftank/lib/webcam"

	"gobot.io/x/gobot"
)

func main() {
	var addr = flag.String("addr", ":8080", "http service address")
	var webcamID = flag.Int("webcamID", 0, "Webcam device ID.")
	var webcamEnabled = flag.Bool("webcam", true, "Enable Webcam.")

	flag.Parse()

	com := make(chan *ws.BotCommand)
	h := ws.NewHub(com)
	go h.Run()
	defer h.Stop()

	var gracefulStop = make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)

	work := func() {
		go func() {
			for {
				select {
				case c := <-com:
					// TODO: Input validation
					if c.Motor != nil {
						log.Printf("Set Left Motor %f, Set Right Motor %f", c.Motor.Left, c.Motor.Right)
					}
					log.Printf("Set Shooter: %s", c.Shooter)
					log.Printf("Shooter Command: %s", c.ShooterCommand)
					if c.Event == "Disconnect" {
						log.Println("Stop Robot")
					}
				case <-gracefulStop:
					log.Println("Stop Work")
					return
				}
			}
		}()
	}

	robot := gobot.NewRobot("nerftank",
		[]gobot.Connection{},
		[]gobot.Device{},
		work,
	)

	go robot.Start()
	defer robot.Stop()

	log.Println("Robot started")

	mux := http.NewServeMux()
	srv := &http.Server{
		Addr:    *addr,
		Handler: mux,
	}

	if *webcamEnabled {
		w := webcam.NewCamera(*webcamID)
		w.Start(true)
		defer w.Stop()
		mux.Handle("/webcam", w.Stream())
	}
	mux.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) { h.ServeWs(w, r) })
	assets := packr.NewBox("../assets")
	mux.Handle("/", http.FileServer(assets))

	log.Println("Start webserver")

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			log.Fatal("ListenAndServe: ", err)
		}
	}()

	<-gracefulStop

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*1)
	defer cancel()

	err := srv.Shutdown(ctx)
	if err != nil {
		log.Println(err)
	}

	// - Sensor output (broadcast)
}

// TODO: Follow me action
// TODO: Beleuchtung / Nightrider
