# Nerftank

The nerftank is a remote controlled robot able to fire nerf darts using flywheels. The Gun is 3D printend an can be easy mounted on different platforms.

The electronics are build aound an raspberry pi zero. The software is written in golang and provides a remote control interface in the browser using websocket connections and a mjpeg video stream.

# Things

- Devastator
- Raspberry Pi zero W
- Rasperry Pi Camera
- Piborg Reverse (drive motor controller)
- Piborg Batt (powering the electronics)
- 16-Channel Servo Controller (PCA9685 from Adafruit)
- Battery
- Servos
- Flywheel Controller
- Some screws

# Assembly

# Schematics
# Code

# Development
- Building the software
- Testing

